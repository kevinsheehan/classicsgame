package org.ksdev.classic;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class DesktopLauncher {
	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "ClassicClone";
		cfg.useGL20 = false;
		cfg.width = 960;
		cfg.height = 540;
		
		new LwjglApplication(new ClassicClone(), cfg);
	}
}
