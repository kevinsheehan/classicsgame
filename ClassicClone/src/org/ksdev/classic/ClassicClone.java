package org.ksdev.classic;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.FPSLogger;
import org.ksdev.classic.assets.Audio;
import org.ksdev.classic.assets.AudioFX;
import org.ksdev.classic.model.Region;
import org.ksdev.classic.assets.Art;
import org.ksdev.classic.screens.GameScreen;

public class ClassicClone extends Game {
    public static final String LOG = ClassicClone.class.getSimpleName();
    public static final boolean DEBUG = true;
    private FPSLogger fpsLogger;
	
	@Override
	public void create() {
        if (DEBUG)
            Gdx.app.setLogLevel(Application.LOG_DEBUG);
        else
            Gdx.app.setLogLevel(Application.LOG_INFO);
        Gdx.app.log(ClassicClone.LOG, "Loading...");
        Gdx.app.debug(ClassicClone.LOG, "Loading art...");
        Art.load();
        Gdx.app.debug(ClassicClone.LOG, "Loading audio...");
        Audio.load();
        AudioFX.load();
        Gdx.app.debug(ClassicClone.LOG, "Loading definitions...");
        Gdx.app.debug(ClassicClone.LOG, "Loading scripts...");
        Gdx.app.log(ClassicClone.LOG, "Starting game...");
        setScreen(new GameScreen(new Region(Art.tutorial)));
        fpsLogger = new FPSLogger();
	}

	@Override
	public void dispose() {
        super.dispose();
	}

	/*@Override
	public void render() {
        super.render();
        if (DEBUG)
            fpsLogger.log();
	}*/

	@Override
	public void resize(int width, int height) {
        super.resize(width, height);
        Gdx.app.log(ClassicClone.LOG, "Resizing game to: " + width + " x " + height);
	}
}
