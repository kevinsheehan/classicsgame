package org.ksdev.classic.assets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;

/**
 * @author Kevin
 */
public class Audio {
    public static void load() { }

    private static Music load(String name) {
        return Gdx.audio.newMusic(Gdx.files.internal(name));
    }
}
