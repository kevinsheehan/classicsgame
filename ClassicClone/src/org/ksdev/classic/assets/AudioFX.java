package org.ksdev.classic.assets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;

/**
 * @author Kevin
 */
public class AudioFX {
    public static void load() {
    }

    private static Sound load(String name) {
        return Gdx.audio.newSound(Gdx.files.internal(name));
    }
}