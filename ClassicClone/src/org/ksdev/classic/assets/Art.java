package org.ksdev.classic.assets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * @author Kevin
 */
public class Art {
    public static Pixmap tutorial;
    public static TextureRegion[] moveClick;
    public static TextureRegion[] actionClick;

    public static void load() {
        tutorial = new Pixmap(Gdx.files.internal("assets/art/tutorial_small.png"));
        TextureRegion[][] clickMap = loadClickMap();
        moveClick = clickMap[0];
        actionClick = clickMap[1];
    }

    private static TextureRegion[][] loadClickMap() {
        Texture texture = new Texture(Gdx.files.internal("assets/art/click_map.png"));
        int ySlices = 2;
        int xSlices = 5;
        TextureRegion[][] res = new TextureRegion[ySlices][xSlices];
        for (int y = 0; y < ySlices; y++) {
            for (int x = 0; x < xSlices; x++) {
                res[y][x] = new TextureRegion(texture, x * 16, y * 16, 16, 16);
            }
        }
        return res;
    }

    private static TextureRegion[][] split(String name, int width, int height) {
        return split(name, width, height, false);
    }

    private static TextureRegion[][] split(String name, int width, int height, boolean flipX) {
        Texture texture = new Texture(Gdx.files.internal(name));
        int xSlices = texture.getWidth() / width;
        int ySlices = texture.getHeight() / height;
        TextureRegion[][] res = new TextureRegion[xSlices][ySlices];
        for (int x = 0; x < xSlices; x++) {
            for (int y = 0; y < ySlices; y++) {
                res[x][y] = new TextureRegion(texture, x * width, y * height, width, height);
                res[x][y].flip(flipX, true);
            }
        }
        return res;
    }

    public static TextureRegion load(String name, int width, int height) {
        Texture texture = new Texture(Gdx.files.internal(name));
        TextureRegion region = new TextureRegion(texture, 0, 0, width, height);
        region.flip(false, true);
        return region;
    }
}