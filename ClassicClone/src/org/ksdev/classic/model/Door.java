package org.ksdev.classic.model;

import org.ksdev.classic.model.defs.DoorDef;

/**
 * @author Kevin
 */
public class Door extends GameObject {
    private DoorDef def;

    public Door(DoorDef def) {
        this.def = def;
    }
}
