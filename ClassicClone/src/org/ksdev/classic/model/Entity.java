package org.ksdev.classic.model;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector3;

/**
 * @author Kevin
 */
public class Entity {
    private Vector3 position;
    private Texture texture;
    private Region region;

    public Entity(Texture texture, Vector3 position, Region region) {
        this.texture = texture;
        this.position = position;
        this.region = region;
    }

    public Vector3 getPosition() { return position; }
    public Texture getTexture() { return texture; }
    public Region getRegion() { return region; }

    public void setPosition(Vector3 position) {
        this.position = position;
    }

    public final boolean withinRange(Entity e, int radius) {
        return withinRange(e.getPosition(), radius);
    }
    public final boolean withinRange(Vector3 p, int radius) {
        int xDiff = (int)Math.abs(position.x - p.x);
        int yDiff = (int)Math.abs(position.y - p.y);
        return xDiff <= radius && yDiff <= radius;
    }
}
