package org.ksdev.classic.model;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import org.ksdev.classic.model.defs.NpcDef;

import java.util.*;

/**
 * @author Kevin
 */
public class Region {
    private Pixmap level;
    //private TileMap;
    private Map<Vector2, Tile> tileMap;

    private Map<Mob, Tile> mobs = new HashMap<Mob, Tile>(50);
    private Player player;

    public Region(Pixmap level) {
        this.level = level;
        tileMap = new HashMap<Vector2, Tile>(level.getHeight()*level.getWidth());
        generateTileMap();
        player = new Player(new Vector3(30, 30, 0), this);
        addNpc(new Npc(new NpcDef(0, 5), new Texture("assets/art/cow.png"), new Vector3(40, 18, 0), this));
    }

    public void update(float delta) {
        player.update(delta);
        for (Mob mob : mobs.keySet()) {
            mob.update(delta);
        }
    }

    public void generateTileMap() {
        Color color = new Color();
        Color stoneWall = Color.valueOf("606260");
        Color water = Color.valueOf("1f3f7d");
        Color door = Color.valueOf("af5f00");
        for (int i = 0; i < level.getWidth(); i++) {
            for (int j = 0; j < level.getHeight(); j++) {
                try {
                    Color.rgba8888ToColor(color, level.getPixel(i, level.getHeight() - j - 1));
                    if (color.equals(door)) {
                        tileMap.put(new Vector2(i, j), new Tile(i, j, true, new GameObject()));
                    } else if (!color.equals(stoneWall) && !color.equals(water)) {
                        tileMap.put(new Vector2(i, j), new Tile(i, j));
                    }
                } catch(Exception e) {
                }
            }
        }
    }

    public Tile getTile(Vector2 position) {
        return tileMap.get(position);
    }

    public void addNpc(Npc npc) {
        Tile tile = tileMap.get(new Vector2(npc.getPosition().x, npc.getPosition().y));
        tile.addMob(npc);
        mobs.put(npc, tile);
    }

    public void updateNpc(Npc npc, Vector2 oldLocation) {
        Tile tile = tileMap.get(oldLocation);
        tile.removeMob(npc);
        tile = tileMap.get(new Vector2(npc.getPosition().x, npc.getPosition().y));
        mobs.put(npc, tile);
    }

    public void removeNpc(Npc npc) {
        Tile tile = tileMap.get(new Vector2(npc.getPosition().x, npc.getPosition().y));
        tile.removeMob(npc);
        mobs.remove(npc);
    }

    public Pixmap getLevel() { return level; }
    public Player getPlayer() { return player; }
    public Set<Mob> getMobs() { return mobs.keySet(); }

    public int getHeight() { return level.getHeight(); }
    public int getWidth() { return level.getWidth(); }
}
