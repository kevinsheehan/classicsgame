package org.ksdev.classic.model;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector3;

/**
 * @author Kevin
 */
public abstract class Mob extends Entity {
    private PathHandler pathHandler = new PathHandler(this);
    private long lastUpdate = System.nanoTime();
    private float deltaTime = 0;

    public Mob(Texture texture, Vector3 position, Region region) {
        super(texture, position, region);
    }

    public void setPath(Path path) {
        pathHandler.setPath(path);
    }

    public PathHandler getPathHandler() { return pathHandler; }

    public void update(float delta) {
        deltaTime += delta;
        long currentTime = System.nanoTime();
        if (currentTime - lastUpdate > 1E9 / 3) {
            lastUpdate = currentTime;
            pathHandler.updatePosition();
        }
    }
}
