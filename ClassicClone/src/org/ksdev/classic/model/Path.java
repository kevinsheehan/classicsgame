package org.ksdev.classic.model;

/**
 * @author Kevin
 */
public class Path {
    private boolean noClip = false;
    private int startX, startY;
    private int[] waypointXoffsets, waypointYoffsets;

    public Path(int startX, int startY, int[] waypointXoffsets,
                int[] waypointYoffsets) {
        this.startX = startX;
        this.startY = startY;
        this.waypointXoffsets = waypointXoffsets;
        this.waypointYoffsets = waypointYoffsets;
    }

    public Path(int x, int y, int endX, int endY) {
        startX = endX;
        startY = endY;
        waypointXoffsets = new int[0];
        waypointYoffsets = new int[0];
    }

    public Path(int x, int y, int endX, int endY, boolean noClip) {
        startX = endX;
        startY = endY;
        waypointXoffsets = new int[0];
        waypointYoffsets = new int[0];
        this.noClip = noClip;
    }

    public int getStartX() {
        return startX;
    }

    public int getStartY() {
        return startY;
    }

    public int getWaypointX(int wayPoint) {
        return startX + getWaypointXoffset(wayPoint);
    }

    public int getWaypointXoffset(int wayPoint) {
        if (wayPoint >= length()) {
            return 0;
        }
        return waypointXoffsets[wayPoint];
    }

    public int getWaypointY(int wayPoint) {
        return startY + getWaypointYoffset(wayPoint);
    }

    public int getWaypointYoffset(int wayPoint) {
        if (wayPoint >= length()) {
            return 0;
        }
        return waypointYoffsets[wayPoint];
    }

    public boolean isNoClip() {
        return noClip;
    }

    public int length() {
        if (waypointXoffsets == null) {
            return 0;
        }
        return waypointXoffsets.length;
    }
}
