package org.ksdev.classic.model;

import org.ksdev.classic.model.defs.NpcDef;

import java.util.ArrayList;

/**
 * @author Kevin
 */
public class Tile {
    private int x, y;
    private ArrayList<Item> items;
    private ArrayList<Npc> npcs;

    private boolean barrier;
    private GameObject object;

    public Tile(int x, int y) {
        this.x = x;
        this.y = y;
        barrier = false;
        object = null;
        items = new ArrayList<Item>();
        npcs = new ArrayList<Npc>();
    }

    public Tile(int x, int y, boolean barrier, GameObject object) {
        this.x = x;
        this.y = y;
        this.barrier = barrier;
        this.object = object;
        items = new ArrayList<Item>();
        npcs = new ArrayList<Npc>();
    }

    public String getTopAction() {
        String action = "";
        if (!npcs.isEmpty()) {
            NpcDef npcDef = npcs.get(0).getDef();
            if (npcDef.friendly && npcDef.talks) {
                action = "Talk to " + npcDef.id;
            } else if (!npcDef.friendly) {
                action = "Attack " + npcDef.id;
            }
        } else if (!items.isEmpty()) {
            action = "Pick-up " + items.get(0).getDef().id;
        } else {
            action = "Walk to " + x + ", " + y;
        }
        return action;
    }

    public void addMob(Npc mob) {
        npcs.add(mob);
    }

    public void removeMob(Npc mob) {
        npcs.remove(mob);
    }

    public void addItem(Item item) {
        items.add(item);
    }

    public void removeItem(Item item) {
        items.remove(item);
    }
}
