package org.ksdev.classic.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector3;
import org.ksdev.classic.ClassicClone;

/**
 * @author Kevin
 */
public class PathHandler {
    public static final String LOG = PathHandler.class.getSimpleName();
    private int curWaypoint;
    private Mob mob;
    private Path path;

    public PathHandler(Mob m) {
        mob = m;
        resetPath();
    }

    protected boolean atStart() {
        return mob.getPosition().x == path.getStartX() && mob.getPosition().y == path.getStartY();
    }

    protected boolean atWaypoint(int waypoint) {
        return path.getWaypointX(waypoint) == mob.getPosition().x
                && path.getWaypointY(waypoint) == mob.getPosition().y;
    }

    private int[] cancelCoords() {
        resetPath();
        return new int[] { -1, -1 };
    }

    public boolean finishedPath() {
        if (path == null) {
            return true;
        }
        if (path.length() > 0) {
            return atWaypoint(path.length() - 1);
        } else {
            return atStart();
        }
    }

    /**
     * Gets the next coordinate in the right direction
     */
    protected int[] getNextCoords(int startX, int destX, int startY, int destY) {
        try {
            int[] coords = { startX, startY };
            boolean myXBlocked = false, myYBlocked = false, newXBlocked = false, newYBlocked = false;
            if (startX > destX) {
                myXBlocked = isBlocking(startX - 1, startY); // Check right
                // tiles left
                // wall
                coords[0] = startX - 1;
            } else if (startX < destX) {
                myXBlocked = isBlocking(startX + 1, startY); // Check left
                // tiles right
                // wall
                coords[0] = startX + 1;
            }

            if (startY > destY) {
                myYBlocked = isBlocking(startX, startY - 1); // Check top
                // tiles bottom
                // wall
                coords[1] = startY - 1;
            } else if (startY < destY) {
                myYBlocked = isBlocking(startX, startY + 1); // Check bottom
                // tiles top
                // wall
                coords[1] = startY + 1;
            }

            // If both directions are blocked OR we are going straight and the
            // direction is blocked
            if ((myXBlocked && myYBlocked) || (myXBlocked && startY == destY)
                    || (myYBlocked && startX == destX)) {
                return cancelCoords();
            }

            if (coords[0] > startX) {
                newXBlocked = isBlocking(coords[0], coords[1]); // Check dest
                // tiles
                // right
                // wall
            } else if (coords[0] < startX) {
                newXBlocked = isBlocking(coords[0], coords[1]); // Check dest
                // tiles
                // left wall
            }

            if (coords[1] > startY) {
                newYBlocked = isBlocking(coords[0], coords[1]); // Check dest
                // tiles top
                // wall
            } else if (coords[1] < startY) {
                newYBlocked = isBlocking(coords[0], coords[1]); // Check dest
                // tiles
                // bottom
                // wall
            }

            // If both directions are blocked OR we are going straight and the
            // direction is blocked
            if ((newXBlocked && newYBlocked)
                    || (newXBlocked && startY == coords[1])
                    || (myYBlocked && startX == coords[0])) {
                return cancelCoords();
            }

            // If only one direction is blocked, but it blocks both tiles
            if ((myXBlocked && newXBlocked) || (myYBlocked && newYBlocked)) {
                return cancelCoords();
            }

            return coords;
        } catch (Exception e) {
            return cancelCoords();
        }
    }

    private boolean isBlocking(int val) {
        Color tile = new Color();
        Color.rgba8888ToColor(tile, val);
        Color stoneWall = Color.valueOf("606260");
        Color water = Color.valueOf("1f3f7d");
        if (tile.equals(stoneWall)) {
            return true;
        } else if (tile.equals(water)) {
            return true;
        } /*else if (val = "af5f00") { // closed door
            return true;
        }*/
        return false;
    }

    private boolean isBlocking(int x, int y) {
        int tileValue = mob.getRegion().getLevel().getPixel(x, mob.getRegion().getLevel().getHeight()-y-1);
        return isBlocking(tileValue);
    }

    /**
     * Resets the path (stops movement)
     */
    protected void resetPath() {
        path = null;
        curWaypoint = -1;
    }

    /**
     * Updates our position to the next in the path
     */
    protected void setNextPosition() {
        int[] newCoords = { -1, -1 };
        if (curWaypoint == -1) {
            if (atStart()) {
                curWaypoint = 0;
            } else {
                newCoords = getNextCoords((int)mob.getPosition().x, path.getStartX(),
                        (int)mob.getPosition().y, path.getStartY());
            }
        }
        if (curWaypoint > -1) {
            if (atWaypoint(curWaypoint)) {
                curWaypoint++;
            }
            if (curWaypoint < path.length()) {
                newCoords = getNextCoords((int)mob.getPosition().x, path.getWaypointX(curWaypoint),
                        (int)mob.getPosition().y, path.getWaypointY(curWaypoint));
            } else {
                resetPath();
            }
        }
        if (newCoords[0] > -1 && newCoords[1] > -1) {
            mob.setPosition(new Vector3(newCoords[0], newCoords[1], mob.getPosition().z));
        }
    }

    public void setPath(int startX, int startY, int[] waypointXoffsets,
                        int[] waypointYoffsets) {
        setPath(new Path(startX, startY, waypointXoffsets, waypointYoffsets));
    }

    public void setPath(Path path) {
        curWaypoint = -1;
        this.path = path;
    }

    public Path createPath(int startX, int startY, int endX, int endY) {
        return createPath(startX, startY, endX, endY, endX, endY, false);
    }

    public Path createPath(int startX, int startY, int endX, int endY, boolean adjacentStop) {
        return createPath(startX, startY, endX, endY, endX, endY, adjacentStop);
    }

    public Path createPath(int startX, int startY, int endXmin, int endYmin, int endXmax, int endYmax) {
        return createPath(startX, startY, endXmin, endYmin, endXmax, endYmax, false);
    }

    public Path createPath(int startX, int startY, int endXmin, int endYmin, int endXmax, int endYmax, boolean adjacentStop) {
        int mapWidth = mob.getRegion().getWidth();
        int mapHeight = mob.getRegion().getHeight();
        int[][] pathMap = new int[mapWidth][mapHeight];
        for (int i = 0; i < mapWidth; i++) {
            for (int j = 0; j < mapHeight; j++) {
                pathMap[i][j] = 0;
            }
        }

        int[] xWaypoints = new int[200];
        int[] yWaypoints = new int[200];
        int nextStep = 0;
        int currentStep = 0;
        int currentX = startX;
        int currentY = startY;
        pathMap[startX][startY] = 99;
        xWaypoints[nextStep] = startX;
        yWaypoints[nextStep++] = startY;
        int MAX_ARRAY_SIZE = xWaypoints.length;
        boolean reachedGoal = false;
        //walk at tile
        while (currentStep != nextStep) {
            currentX = xWaypoints[currentStep];
            currentY = yWaypoints[currentStep];
            currentStep = (currentStep + 1) % MAX_ARRAY_SIZE;
            if (currentX >= endXmin && currentX <= endXmax && currentY >= endYmin && currentY <= endYmax) {
                reachedGoal = true;
                break;
            }
            if (adjacentStop) {
                if (currentX > 0 && currentX - 1 >= endXmin && currentX - 1 <= endXmax && currentY >= endYmin
                        && currentY <= endYmax && !isBlocking(currentX - 1, currentY)) {
                    reachedGoal = true;
                    break;
                }
                if (currentX < mapWidth && currentX + 1 >= endXmin && currentX + 1 <= endXmax && currentY >= endYmin
                        && currentY <= endYmax && !isBlocking(currentX + 1, currentY)) {
                    reachedGoal = true;
                    break;
                }
                if (currentY > 0 && currentX >= endXmin && currentX <= endXmax && currentY - 1 >= endYmin
                        && currentY - 1 <= endYmax && !isBlocking(currentX, currentY - 1)) {
                    reachedGoal = true;
                    break;
                }
                if (currentY < mapHeight && currentX >= endXmin && currentX <= endXmax && currentY + 1 >= endYmin
                        && currentY + 1 <= endYmax && !isBlocking(currentX, currentY + 1)) {
                    reachedGoal = true;
                    break;
                }
            }
            // Tile hasn't been reached yet, can we reach it from here?
            // Reachable from right
            if (currentX > 0 && pathMap[currentX - 1][currentY] == 0
                    && !isBlocking(currentX - 1, currentY)) {
                xWaypoints[nextStep] = currentX - 1;
                yWaypoints[nextStep] = currentY;
                nextStep = (nextStep + 1) % MAX_ARRAY_SIZE;
                pathMap[currentX - 1][currentY] = 2;
            }
            // Reachable from left
            if (currentX < mapWidth && pathMap[currentX + 1][currentY] == 0
                    && !isBlocking(currentX + 1, currentY)) {
                xWaypoints[nextStep] = currentX + 1;
                yWaypoints[nextStep] = currentY;
                nextStep = (nextStep + 1) % MAX_ARRAY_SIZE;
                pathMap[currentX + 1][currentY] = 8;
            }
            // Reachable from below
            if (currentY > 0 && pathMap[currentX][currentY - 1] == 0
                    && !isBlocking(currentX, currentY - 1)) {
                xWaypoints[nextStep] = currentX;
                yWaypoints[nextStep] = currentY - 1;
                nextStep = (nextStep + 1) % MAX_ARRAY_SIZE;
                pathMap[currentX][currentY - 1] = 1;
            }
            // Reachable from above
            if (currentY < mapHeight && pathMap[currentX][currentY + 1] == 0
                    && !isBlocking(currentX, currentY + 1)) {
                xWaypoints[nextStep] = currentX;
                yWaypoints[nextStep] = currentY + 1;
                nextStep = (nextStep + 1) % MAX_ARRAY_SIZE;
                pathMap[currentX][currentY + 1] = 4;
            }
            // Diagonal reachables
            // All tiles need to be reachable:
            // player = p, x = goal, r = reachable tiles
            // p r
            // r x
            if (currentX > 0 && currentY > 0
                    && !isBlocking(currentX, currentY - 1)
                    && !isBlocking(currentX - 1, currentY)
                    && !isBlocking(currentX - 1, currentY - 1)
                    && pathMap[currentX - 1][currentY - 1] == 0) {
                xWaypoints[nextStep] = currentX - 1;
                yWaypoints[nextStep] = currentY - 1;
                nextStep = (nextStep + 1) % MAX_ARRAY_SIZE;
                pathMap[currentX - 1][currentY - 1] = 3;
            }
            if (currentX < mapWidth && currentY > 0
                    && !isBlocking(currentX, currentY - 1)
                    && !isBlocking(currentX + 1, currentY)
                    && !isBlocking(currentX + 1, currentY - 1)
                    && pathMap[currentX + 1][currentY - 1] == 0) {
                xWaypoints[nextStep] = currentX + 1;
                yWaypoints[nextStep] = currentY - 1;
                nextStep = (nextStep + 1) % MAX_ARRAY_SIZE;
                pathMap[currentX + 1][currentY - 1] = 9;
            }
            if (currentX > 0 && currentY < mapHeight
                    && !isBlocking(currentX, currentY + 1)
                    && !isBlocking(currentX - 1, currentY)
                    && !isBlocking(currentX - 1, currentY + 1)
                    && pathMap[currentX - 1][currentY + 1] == 0) {
                xWaypoints[nextStep] = currentX - 1;
                yWaypoints[nextStep] = currentY + 1;
                nextStep = (nextStep + 1) % MAX_ARRAY_SIZE;
                pathMap[currentX - 1][currentY + 1] = 6;
            }
            if (currentX < mapWidth && currentY < mapHeight
                    && !isBlocking(currentX, currentY + 1)
                    && !isBlocking(currentX + 1, currentY)
                    && !isBlocking(currentX + 1, currentY + 1)
                    && pathMap[currentX + 1][currentY + 1] == 0) {
                xWaypoints[nextStep] = currentX + 1;
                yWaypoints[nextStep] = currentY + 1;
                nextStep = (nextStep + 1) % MAX_ARRAY_SIZE;
                pathMap[currentX + 1][currentY + 1] = 12;
            }
        }
        if (!reachedGoal) {
            return new Path(startX, startY, startX, startY);
        }
        currentStep = 0;
        xWaypoints[currentStep] = currentX;
        yWaypoints[currentStep++] = currentY;
        int currentStepDirection;
        // Once we change direction we add a new waypoint
        for (int nextStepDirection = currentStepDirection = pathMap[currentX][currentY]; currentX != startX
                || currentY != startY; nextStepDirection = pathMap[currentX][currentY]) {
            if (nextStepDirection != currentStepDirection) {
                currentStepDirection = nextStepDirection;
                xWaypoints[currentStep] = currentX;
                yWaypoints[currentStep++] = currentY;
            }
            if ((nextStepDirection & 2) != 0) {
                currentX++;
            } else if ((nextStepDirection & 8) != 0) {
                currentX--;
            }
            if ((nextStepDirection & 1) != 0) {
                currentY++;
            } else if ((nextStepDirection & 4) != 0) {
                currentY--;
            }
        }
        int[] xOffsets = new int[currentStep+1];
        int[] yOffsets = new int[currentStep+1];
        for (int i = 1; i <= currentStep; i++) {
            xOffsets[i] = xWaypoints[currentStep - i] - startX;
            yOffsets[i] = yWaypoints[currentStep - i] - startY;
        }
        return new Path(startX, startY, xOffsets, yOffsets);
    }

    public void updatePosition() {
        if (!finishedPath()) {
            setNextPosition();
            Gdx.app.debug(PathHandler.LOG, "Updated position: " + mob.getPosition().toString());
        }
    }
}
