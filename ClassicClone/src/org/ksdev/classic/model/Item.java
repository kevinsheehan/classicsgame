package org.ksdev.classic.model;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector3;
import org.ksdev.classic.model.defs.ItemDef;

/**
 * @author Kevin
 */
public class Item extends Entity {
    private ItemDef def;

    public Item(ItemDef def,  Texture texture, Vector3 position, Region region) {
        super(texture, position, region);
        this.def = def;
    }

    public ItemDef getDef() { return def; }
}
