package org.ksdev.classic.model;

/**
 * @author Kevin
 */
public enum Skill {
    Attack(1), Strength(2), Defense(3), Mining(4), Woodcutting(5), Fishing(6);

    public int max = 99;
    public int id;

    private Skill(int id) {
        this.id = id;
    }
}
