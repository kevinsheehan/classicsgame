package org.ksdev.classic.model.defs;

/**
 * @author Kevin
 */
public class ItemDef {
    public int id;
    public boolean stackable;
    public int quantity;
    public boolean quest;

    public ItemDef(int id) {
        this.id = id;
        this.quantity = 1;
        this.stackable = false;
    }

    public ItemDef(int id, boolean stackable, int quantity) {
        this.id = id;
        this.stackable = stackable;
        this.quantity = quantity;
    }
}
