package org.ksdev.classic.model.defs;

import org.ksdev.classic.model.CombatStats;

/**
 * @author Kevin
 */
public class NpcDef {
    public int id;
    public boolean friendly;
    public boolean talks;
    public int level;
    public CombatStats stats;
    public String name = "Cow";

    public NpcDef(int id) {
        this.id = id;
        friendly = true;
        talks = true;
    }
    public NpcDef(int id, int level) {
        this.id = id;
        friendly = false;
        talks = false;
        this.level = level;
    }
}
