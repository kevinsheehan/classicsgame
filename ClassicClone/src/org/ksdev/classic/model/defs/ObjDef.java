package org.ksdev.classic.model.defs;

import org.ksdev.classic.model.Skill;

/**
 * @author Kevin
 */
public class ObjDef {
    public boolean skillRelated;
    public Skill skill;
    public int id;
    public boolean interactive;
    public String action;

    public int resetTime; // -1 for never
}
