package org.ksdev.classic.model.defs;

/**
 * @author Kevin
 */
public class DoorDef extends ObjDef {
    public boolean open;
    public boolean locked;
    public int keyId;
}
