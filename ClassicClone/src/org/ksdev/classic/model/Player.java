package org.ksdev.classic.model;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector3;

/**
 * @author Kevin
 */
public final class Player extends Mob {
    private long lastUpdate = System.nanoTime();
    private float deltaTime = 0;

    public Player(Vector3 position, Region region) {
        super(new Texture("assets/art/player.png"), position, region);
    }

    public void update(float delta) {
        super.update(delta);
        deltaTime += delta;
        /*long currentTime = System.nanoTime();
        if (currentTime - lastUpdate > 1E9) {
            lastUpdate = currentTime;
        }*/
    }
}
