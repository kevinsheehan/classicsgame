package org.ksdev.classic.model;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector3;
import org.ksdev.classic.model.defs.NpcDef;

/**
 * Shop owners, quest givers
 * @author Kevin
 */
public class Npc extends Mob {
    private NpcDef def;

    public Npc(NpcDef def, Texture texture, Vector3 position, Region region) {
        super(texture, position, region);
        this.def = def;
    }

    public NpcDef getDef() { return def; }
}
