package org.ksdev.classic.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.math.Vector2;
import org.ksdev.classic.model.Path;
import org.ksdev.classic.model.Region;
import org.ksdev.classic.render.Renderer;

/**
 * @author Kevin
 */
public class GameScreen implements Screen, InputProcessor {
    private Region region;
    private float deltaTime;
    private Renderer renderer;
    private int width, height;

    public GameScreen(Region region) {
        this.region = region;
        renderer = new Renderer(region);
        Gdx.input.setInputProcessor(this);
    }

    @Override
    public void render(float delta) {
        region.update(delta);

        deltaTime += delta;
        if (deltaTime > 0.01) {
            Gdx.gl.glClearColor(0.1f, 0.1f, 0.1f, 1f);
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
            deltaTime = 0;
            renderer.render();
        }
    }

    @Override
    public void resize(int width, int height) {
        this.width = width;
        this.height = height;
        renderer.setSize(width, height);
    }

    @Override
    public void show() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
        Gdx.input.setInputProcessor(null);
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if (button == Input.Buttons.LEFT) {
            Vector2 point = renderer.screenToPoint(screenX, height-screenY);
            renderer.addClick(new Vector2(screenX, height-screenY), Renderer.ClickType.Walk);
            region.getPlayer().setPath(region.getPlayer().getPathHandler().createPath(
                    (int)region.getPlayer().getPosition().x,
                    (int)region.getPlayer().getPosition().y,
                    (int)point.x, (int)point.y));
        }
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
