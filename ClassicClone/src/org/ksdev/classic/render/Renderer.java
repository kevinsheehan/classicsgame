package org.ksdev.classic.render;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import org.ksdev.classic.ClassicClone;
import org.ksdev.classic.assets.Art;
import org.ksdev.classic.model.Mob;
import org.ksdev.classic.model.Player;
import org.ksdev.classic.model.Region;
import org.ksdev.classic.model.Tile;
import org.ksdev.common.domain.Pair;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Kevin
 */
public class Renderer {
    public static final String LOG = Renderer.class.getSimpleName();

    private static final float CAMERA_WIDTH = 27f;
    private static final float CAMERA_HEIGHT = 15f;
    private static final float SIZE = 1f;
    private int width, height;
    private float xOffset, yOffset;
    private Region region;
    private OrthographicCamera camera;

    private ShapeRenderer debugRenderer = new ShapeRenderer();
    private ShapeRenderer mapRenderer = new ShapeRenderer();
    private SpriteBatch spriteBatch = new SpriteBatch();
    private SpriteBatch screenText = new SpriteBatch();
    private BitmapFont font = new BitmapFont();
    private BitmapFont actionFont = new BitmapFont();

    private Map<Pair<Animation, Vector2>, Long> screenFx = new ConcurrentHashMap<Pair<Animation, Vector2>, Long>();


    public enum ClickType { Walk, Action }

    /**
     * Creates a new instance of a Renderer.
     * @param region The region that we are rendering to the screen.
     */
    public Renderer(Region region) {
        this.region = region;
        camera = new OrthographicCamera(CAMERA_WIDTH, CAMERA_HEIGHT);
        camera.position.set(23f, 23f, 0f);
    }

    public void addClick(Vector2 pos, ClickType type) {
        TextureRegion[] textures = type == ClickType.Walk ? Art.moveClick : Art.actionClick;
        pos.x -= 8;
        pos.y -= 8;
        screenFx.put(new Pair<Animation, Vector2>(new Animation(0.1f, textures), pos), System.currentTimeMillis());
    }

    public void setSize(int width, int height) {
        this.width = width;
        this.height = height;
    }

    /**
     * The render method needs to maintain the order we want things drawn onto the screen.
     * <ol>
     *     <li>Map</li>
     *     <li>Objects</li>
     *     <li>Items</li>
     *     <li>Mobs</li>
     *     <li>Player</li>
     *     <li>Clicks</li>
     *     <li>Overlay Text/HUD</li>
     * </ol>
     */
    public void render() {
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        moveCamera(region.getPlayer().getPosition().x, region.getPlayer().getPosition().y);
        drawMap(); // if we aren't actually modifying or drawing anything that changes the map, this can be done on camera move
        spriteBatch.setProjectionMatrix(camera.combined);
        spriteBatch.begin();
        drawMobs();
        drawPlayer();
        spriteBatch.end();
        screenText.begin();
        drawScreenFx();
        drawHud();
        screenText.end();
        if (ClassicClone.DEBUG)
            drawDebug();
    }

    public void moveCamera(float x, float y) {
        if (camera.position.x == x && camera.position.y == y) return;
        if (x > CAMERA_WIDTH/2 && x < region.getWidth()-(CAMERA_WIDTH/2)) {
            camera.position.x = x;
            xOffset = x - CAMERA_WIDTH/2;
        }  else if (x < CAMERA_WIDTH/2) {
            camera.position.x = CAMERA_WIDTH/2;
            xOffset = 0;
        }  else if (x > region.getWidth() - CAMERA_WIDTH/2) {
            camera.position.x = region.getWidth() - CAMERA_WIDTH/2;
            xOffset = region.getWidth() - CAMERA_WIDTH;
        }
        if (y > CAMERA_HEIGHT/2 && y < region.getHeight()-(CAMERA_HEIGHT/2)) {
            camera.position.y = y;
            yOffset = y - CAMERA_HEIGHT/2;
        } else if (y < CAMERA_HEIGHT/2) {
            camera.position.y = CAMERA_HEIGHT/2;
            yOffset = 0;
        }  else if (y > region.getHeight() - CAMERA_HEIGHT/2) {
            camera.position.y = region.getHeight() - CAMERA_HEIGHT/2;
            yOffset = region.getHeight() - CAMERA_HEIGHT;
        }
        camera.update();
    }

    /**
     * Grabs the Pixmap of the region and then turns each pixel of the map into a tile on the display.
     */
    /*private void drawMap() {
        Pixmap map = region.getLevel();
        mapRenderer.setProjectionMatrix(camera.combined);
        mapRenderer.begin(ShapeRenderer.ShapeType.FilledRectangle);
        Color paint = new Color();
        int EDGE_BUFFER = 1;
        int y1 = (int)(camera.position.y - CAMERA_HEIGHT/2 - EDGE_BUFFER);
        y1 = (y1 > 0) ? y1 : 0;
        int y2 = (int)(camera.position.y + CAMERA_HEIGHT/2 + EDGE_BUFFER);
        y2 = (y2 < map.getHeight()) ? y2 : map.getHeight();
        int x1 = (int)(camera.position.x - CAMERA_WIDTH/2 - EDGE_BUFFER);
        x1 = (x1 > 0) ? x1 : 0;
        int x2 = (int)(camera.position.x + CAMERA_WIDTH/2 + EDGE_BUFFER);
        x2 = (x2 < map.getWidth()) ? x2 : map.getWidth();
        for(int i = y1; i < y2; i++) {
            for(int j = x1; j < x2; j++) {
                try {
                    Color.rgba8888ToColor(paint, map.getPixel(j, y2-i-1));
                    mapRenderer.setColor(paint);
                    mapRenderer.filledRect(j, i, SIZE, SIZE);
                } catch(Exception e) {
                }
            }
        }
        mapRenderer.end();
    }*/
    private void drawMap() {
        Pixmap map = region.getLevel();
        mapRenderer.setProjectionMatrix(camera.combined);
        mapRenderer.begin(ShapeRenderer.ShapeType.FilledRectangle);
        Color paint = new Color();
        for (int i = 0; i < region.getHeight(); i++) {
            for (int j = 0; j < region.getWidth(); j++) {
                try {
                    Color.rgba8888ToColor(paint, map.getPixel(j, region.getHeight()-i-1));
                    mapRenderer.setColor(paint);
                    mapRenderer.filledRect(j, i, SIZE, SIZE);
                } catch(Exception e) {
                }
            }
        }
        mapRenderer.end();
    }

    private void drawMobs() {
        for (Mob mob : region.getMobs()) {
            spriteBatch.draw(mob.getTexture(), mob.getPosition().x, mob.getPosition().y, SIZE, SIZE);
        }
    }

    private void drawPlayer() {
        Player player = region.getPlayer();
        spriteBatch.draw(player.getTexture(), player.getPosition().x, player.getPosition().y, SIZE, SIZE);
    }

    private void drawScreenFx() {
        for (Pair<Animation, Vector2> fx : screenFx.keySet()) {
            long currentTime = System.currentTimeMillis();
            if (fx.getFirst().isAnimationFinished((float)(currentTime-screenFx.get(fx))/1000)) {
                screenFx.remove(fx);
                continue;
            }
            screenText.draw(fx.getFirst().getKeyFrame((float)(currentTime-screenFx.get(fx))/1000), fx.getSecond().x, fx.getSecond().y);
        }
    }

    private void drawHud() {
        actionFont.setColor(1f, 1f, 1f, 1f);
        //actionFont.draw(screenText, "Walk-to ", 10, height - 10);
        int mouseX = Gdx.input.getX();
        int mouseY = Gdx.input.getY();
        Vector2 point = screenToPoint(mouseX, height-mouseY);
        //actionFont.setColor(1f, 1f, 0f, 1f);
        Tile tile = region.getTile(point);
        if (tile != null) {
            String action = tile.getTopAction();
            actionFont.draw(screenText, action, 10, height-10);
        }
        //actionFont.draw(screenText, point.x + ", " + point.y, 10+actionFont.getBounds("Walk-to ").width, height-10);
    }

    /**
     * Draw debug will outline the items and objects that are on the screen. This includes objects, items, mobs,
     * and the player.
     */
    private void drawDebug() {
        screenText.begin();
        font.setColor(0f, 1f, 0f, 1f);
        font.draw(screenText, "Debug Mode ", 5, 20);
        font.setColor(1f, 0.2f, 0.2f, 1f);
        font.draw(screenText, "FPS: " + Gdx.graphics.getFramesPerSecond(), 5+font.getBounds("Debug Mode ").width, 20);
        screenText.end();

        debugRenderer.setProjectionMatrix(camera.combined);
        debugRenderer.begin(ShapeRenderer.ShapeType.Rectangle);
        // outline the mobs
        for (Mob mob : region.getMobs()) {
            float x1 = mob.getPosition().x;
            float y1 = mob.getPosition().y;
            debugRenderer.setColor(new Color(1f, 1f, 0f, 1f));
            debugRenderer.rect(x1, y1, SIZE, SIZE);
        }
        // outline the player
        float x1 = region.getPlayer().getPosition().x;
        float y1 = region.getPlayer().getPosition().y;
        debugRenderer.setColor(new Color(1f, 0f, 1f, 1f));
        debugRenderer.rect(x1, y1, SIZE, SIZE);
        debugRenderer.end();
    }

    public Vector2 screenToPoint(int screenX, int screenY) {
        Vector3 point = new Vector3(screenX, height - screenY, 0);
        camera.unproject(point);
        point.x = (float) Math.floor((double) point.x);
        point.y = (float) Math.floor((double) point.y);
        return new Vector2(point.x, point.y);
    }
}
